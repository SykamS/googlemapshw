package com.sjsu.android.googlemapshomework;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class LocationsContentProvider extends ContentProvider {
    private SQLiteDatabase db;
    public static final String PROVIDER_NAME = "com.sjsu.android.googlemapshomework.Maps";
    public static final String URL = "content://" + PROVIDER_NAME + "/locations";
    public static final Uri URI = Uri.parse(URL);

    @Override
    public boolean onCreate() {
        Context context = getContext();
        LocationDB locationDB = new LocationDB(context);
        db = locationDB.getWritableDatabase();
        return (db == null)? false:true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        return db.query(LocationDB.TABLE_NAME, projection, null, null, null, null, null);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long id = db.insert(LocationDB.TABLE_NAME, null, values);
        if(id > 0) {
            Uri _uri = ContentUris.withAppendedId(URI, id);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to add location into" + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = db.delete(LocationDB.TABLE_NAME, null, null);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }


}
