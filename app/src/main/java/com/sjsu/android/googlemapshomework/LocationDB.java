package com.sjsu.android.googlemapshomework;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class LocationDB extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Maps";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME = "locations";

    public static final String PRIMARY_KEY = "Primary_Key";
    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final String ZOOM_LEVEL = "Zoom_Level";

    public static final String TABLE_CREATE = String.format(
            "CREATE TABLE %s (" +
                    " %s integer primary key autoincrement, " +
                    " %s NUMERIC," +
                    " %s NUMERIC," +
                    " %s NUMERIC)",
            TABLE_NAME, PRIMARY_KEY, LATITUDE, LONGITUDE, ZOOM_LEVEL
    );

    public LocationDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void insertToDB(Context context, double latitude, double longitude, float zoomLevel) {
        SQLiteDatabase db = new LocationDB(context).getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put(LocationDB.LATITUDE, latitude);
        newValues.put(LocationDB.LONGITUDE, longitude);
        newValues.put(LocationDB.ZOOM_LEVEL, zoomLevel);
        db.insert(LocationDB.TABLE_NAME, null, newValues);
    }

    public void deleteFromDB(Context context) {
        SQLiteDatabase db = new LocationDB(context).getWritableDatabase();
        db.delete(LocationDB.TABLE_NAME, null, null);
    }

    public ArrayList<Position> getLocationsFromDB(Context context) {
        SQLiteDatabase db = new LocationDB(context).getWritableDatabase();
        String[] resultColumns = {LocationDB.LATITUDE, LocationDB.LONGITUDE, LocationDB.ZOOM_LEVEL};
        Cursor cursor = db.query(LocationDB.TABLE_NAME, resultColumns, null, null, null, null, null);
        return getLocationsFromCursor(cursor);
    }

    public ArrayList<Position> getLocationsFromCursor(Cursor cursor) {
        ArrayList<Position> result = new ArrayList<>();
        while (cursor.moveToNext()){
            result.add(new Position(cursor.getString(0), cursor.getString(1), cursor.getString(2)));
        }
        return result;
    }
}
