package com.sjsu.android.googlemapshomework;

public class Position {

    private String latitude;
    private String longitude;
    private String zoom_level;

    public Position(String latitude, String longitude, String zoom_level) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.zoom_level = zoom_level;
    }
}
