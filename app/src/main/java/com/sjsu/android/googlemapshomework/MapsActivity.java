package com.sjsu.android.googlemapshomework;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LoaderManager.LoaderCallbacks<Cursor> {

    private GoogleMap mMap;
    private LoaderManager lm;
    private final LatLng LOCATION_UNIV = new LatLng(37.335371, -121.881050);
    private final LatLng LOCATION_CS = new LatLng(37.333714, -121.881860);

    private String[] projections = {LocationDB.LATITUDE, LocationDB.LONGITUDE, LocationDB.ZOOM_LEVEL};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        lm = LoaderManager.getInstance(this);
        lm.initLoader(1, null, this);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.addMarker(new MarkerOptions().position(latLng));
                LocationInsertTask insertion = new LocationInsertTask();
                insertion.doInBackground(latLng.latitude, latLng.longitude, Double.parseDouble(new Float(mMap.getCameraPosition().zoom).toString()));
                Toast.makeText(MapsActivity.this,"Marker is added to the Map", Toast.LENGTH_SHORT).show();
            }
        });

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                mMap.clear();
                LocationDeleteTask deletion = new LocationDeleteTask();
                deletion.doInBackground();
                Toast.makeText(MapsActivity.this,"All markers are removed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        return new CursorLoader(MapsActivity.this, LocationsContentProvider.URI, projections, null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        int locationCount = 0;
        double lat = 0;
        double lng = 0;
        float zoom = 0;
        if(data != null) {
            locationCount = data.getCount();
        }

        if(data.moveToFirst()){
            do{
                int latPosition = data.getColumnIndex(LocationDB.LATITUDE);
                int lngPosition = data.getColumnIndex(LocationDB.LONGITUDE);
                int zoomPosition = data.getColumnIndex(LocationDB.ZOOM_LEVEL);
                lat = data.getDouble(latPosition);
                lng = data.getDouble(lngPosition);
                zoom = data.getFloat(zoomPosition);
                LatLng point = new LatLng(lat, lng);
                mMap.addMarker(new MarkerOptions().position(point));
            }while(data.moveToNext());
        }

        if (locationCount > 0) {
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), zoom);
            mMap.animateCamera(update);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }

    private class LocationInsertTask extends AsyncTask<Double, Void, Void> {
        @Override
        protected Void doInBackground(Double... doubles) {
            ContentValues newValues = new ContentValues();
            newValues.put(LocationDB.LATITUDE, doubles[0]);
            newValues.put(LocationDB.LONGITUDE, doubles[1]);
            newValues.put(LocationDB.ZOOM_LEVEL, doubles[2]);
            getContentResolver().insert(LocationsContentProvider.URI, newValues);
            return null;
        }
    }

    private class LocationDeleteTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            getContentResolver().delete(LocationsContentProvider.URI, null, null);
            return null;
        }
    }

    public void onClick_CS(View v) {
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_CS, 18);
        mMap.animateCamera(update);
    }

    public void onClick_Univ(View v) {
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 14);
        mMap.animateCamera(update);
    }

    public void onClick_City(View v) {
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 10);
        mMap.animateCamera(update);
    }
}
